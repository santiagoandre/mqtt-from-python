# python3.6

import random
from time import sleep

from paho.mqtt import client as mqtt_client


broker = 'localhost'
port = 1883
client_id = f'python-caldero-admin'

def publish(client,topic,msg):
    print(f"Send `{msg}` to topic `{topic}`")
    client.publish(topic,msg)
    
def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def enviar_temperatura(client):
    for temp in range(0,150,5):
        publish(client,"temperatura",temp)
        sleep(0.3)

def subscribe(client: mqtt_client,topic):
    def on_message(client, userdata, msg):
        message  = msg.payload.decode()
        vtopic = msg.topic
        if vtopic == "status" and message == "caldero":

            enviar_temperatura(client)
            print("termino el subproceso")

            publish(client,"status","banda")



    client.subscribe(topic)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client,"status")
    client.loop_forever()


if __name__ == '__main__':
    run()
