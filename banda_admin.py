# python3.6

from pydoc import cli
import random
from time import sleep

from paho.mqtt import client as mqtt_client
def publish(client,topic,msg):
    print(f"Send `{msg}` to topic `{topic}`")
    client.publish(topic,msg)
    

broker = 'localhost'
port = 1883
client_id = f'python-banda-admin'


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def embotellar(client):
    for botella in range(1,15,):
        for velocidad in [0,1,2,3,2,1,0]:
            sleep(0.9)
            publish(client,"velocidad",velocidad)
        publish(client,"botellas",botella)
        sleep(0.01)

def subscribe(client: mqtt_client,topic):
    def on_message(client, userdata, msg):
        message  = msg.payload.decode()
        vtopic = msg.topic
        if vtopic == "status" and message == "banda":

            embotellar(client)
            print("termino el subproceso")
            publish(client,"status","off")



    client.subscribe(topic)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client,"status")
    client.loop_forever()


if __name__ == '__main__':
    run()
