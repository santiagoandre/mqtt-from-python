

# python 3.6

import random
import time

from paho.mqtt import client as mqtt_client


broker = 'localhost'
port = 1883
topic = "velocidad"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-client-{random.randint(0, 1000)}'
# username = 'emqx'
# password = 'public'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client):
    msg_count = 0
    add =5
    while True:
        time.sleep(1)
        msg = f"grados {msg_count}"
        result = client.publish(topic, msg_count)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")
        if msg_count >= 250:
            add  = -5
        if msg_count == 50:
            add = 5
        if msg_count == 100:
            add = 15

        msg_count += add
        


def run():
    client = connect_mqtt()
    client.loop_start()
    while True:
        status =input("ingrese el estado del proceso")
        client.publish("status",status)

if __name__ == '__main__':
    run()





